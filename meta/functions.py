import pandas as pd
import numpy as np

class station_upperair():
    ###########################################################################
    ### Unravelling the station's meta information                          ###
    ###########################################################################

    def __init__(self, stationnumber):
        self.stationnumber = stationnumber
        # Initialize object to determine mandatory information for download

    def get_station_information(self):
        # Open upperair file and find line number with specific station
        f = open('meta/stationlist_upperair.txt', 'r').readlines()
        available = False
        for n,line in enumerate(f):
            if self.stationnumber[:5] == line[6:11]:
                available = True
                break
        if not available:
            print('There is no meta information in stationlist_upperair.txt')
            print('about station: '+self.stationnumber)
            exit()

        # Get information
        self.country = f[n][0:3]
        self.lat = float(f[n][12:20])
        self.lon = float(f[n][21:30])
        self.elevation = float(f[n][31:37])
        self.state = f[n][38:40].strip()
        self.name = f[n][41:71].strip()
        self.start = int(f[n][72:76])
        self.end = int(f[n][77:81])
        self.numobs = int(f[n][82:88])
        return