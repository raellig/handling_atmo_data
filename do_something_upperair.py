from meta import functions as meta
import upperair
from glob import glob


path_out = '/data/raffael/dyvalocca/upperair/data/'
path_data = '/data/raffael/dyvalocca/upperair/data/'
path_figures = '/data/raffael/dyvalocca/figures/upperair/meta/'
platforms = ['igra', 'ucar']
stationnumber = '64500'
credentials = {}
credentials['user'] = 'raffael.aellig@kit.edu'
credentials['password'] = '87ZI3t4w'
concat_all_avail = True
#concat_all_avail = ['GBM00064500-data.txt', 'uadb_trhc_64500.txt', \
#'uadb_trh_64500.txt', 'uadb_windc_64500.txt', 'uadb_wind_64500.txt']
stationnumbers = ['64500','64501','64510','64860','64870','64893','64900',\
'64910','64931','64950','65046','65082','65123','65125',\
'65201','65202','65257','64600','64601','64610','64650',\
'64654','64656','64665','64400','64401','64450','64453','64458',\
'64459','64210','64220','66152','66160','66285','66318','66390','66422']
stationnumbers = ['64500','64501','64510']
#stationnumbers = ['10739']

for stationnumber in stationnumbers:
	print('Handling '+stationnumber)
	# Get some Meta data
	station_meta_1 = meta.station_upperair(stationnumber)
	station_meta_1.get_station_information()

	# Download data
	for platform in platforms:
		station_data_1 = upperair.handlingdata.download(path_out=path_out, \
			platform=platform, stationmeta=station_meta_1)
		station_data_1.download_data(force_download=False, \
			credentials=credentials)

	# Concat data
	station_data_1 = upperair.handlingdata.concat(path_out, \
		stationmeta=station_meta_1)
	station_data_1.concat_data(files=concat_all_avail)
quit()


#station_data_1.get_station_information()
#station_data_1.download_data()


data = {}
for stationnumber in stationnumbers:
	meta_data = meta.station_upperair(stationnumber)
	meta_data.get_station_information()

	data[stationnumber] = upperair.analyse.meta(path_data=path_data,
		stationmeta=meta_data)

	data[stationnumber].get_avail_dates_types()

	upperair.analyse.visualize.availability_one_station(data[stationnumber], 
		2018, 2020, path_figures=path_figures)
