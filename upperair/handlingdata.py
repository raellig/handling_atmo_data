from pathlib import Path
from glob import glob
import os
import datetime as dt
from ftplib import FTP
import requests
import sys
import datetime
from timezonefinder import TimezoneFinder
import pytz
from scipy.interpolate import barycentric_interpolate, krogh_interpolate, griddata
import xarray as xr
import numpy as np

class ucar_raw():
	# Class to handle UCAR raw data
	# Datetime: time of release
	# Systemtime: time in utc rounded up to the next 3 hour step
	# Data: raw archived data of the sounding
	# Other definitions see:
	# https://rda.ucar.edu/datasets/ds370.1/docs/UADB-Doc.pdf
	def __init__(self, data):
		self.datetime = datetime.datetime(year=int(data[0][38:42]),
			month=int(data[0][43:45]), day=int(data[0][46:48]))
		if int(data[0][49:53]) == 0:
			pass
		else:
			self.datetime += datetime.timedelta(hours=int(data[0][49:51]),
				minutes=int(float(data[0][51:53])/100*60))
		self.index = data[0][2:14]
		self.stationnumber = data[0][15:21]
		self.station_id_flag = data[0][22:24]
		self.dataset = data[0][25:28]
		self.version_dataset = data[0][29:34]
		self.date_flag = int(data[0][35:37])
		self.location_flag = data[0][54:56]
		self.lat = float(data[0][57:67])
		self.lon = float(data[0][68:78])
		self.elevation = data[0][79:85]
		self.sounding_type = data[0][86:88]
		self.number_levels = data[0][89:93]
		self.product_version = data[0][94:102]
		self.data = data[1:]
		if (self.date_flag >=1) and (self.date_flag <=3):
			self.systemtime = self.datetime + \
			datetime.timedelta(hours=3-self.datetime.hour%3) - \
			datetime.timedelta(minutes=self.datetime.minute)
		elif self.date_flag == 8:
			temp_time = self.datetime + \
				pytz.timezone(TimezoneFinder(in_memory=True).\
				timezone_at(lng=self.lon, lat=self.lat)).\
				localize(self.datetime).utcoffset()
			self.systemtime = temp_time + \
				datetime.timedelta(hours=3-temp_time.hour%3) - \
				datetime.timedelta(minutes=temp_time.minute)


class igra_raw():
	# Class to handle IGRA raw data
	# Datetime: time of release
	# Systemtime: time in utc rounded up to the next 3 hour step
	# Data: raw archived data of the sounding
	# Other definitions see:
	# https://www1.ncdc.noaa.gov/pub/data/igra/data/igra2-data-format.txt
	def __init__(self, data):
		self.systemtime = datetime.datetime(year=int(data[0][13:17]),
			month=int(data[0][18:20]), day=int(data[0][21:23]))
		if (int(data[0][24:26]) >= 0) and (int(data[0][24:26]) <= 23):
			self.systemtime = self.systemtime + \
			datetime.timedelta(hours=int(data[0][24:26]))
		else:
			self.systemtime = self.systemtime + datetime.timedelta(hours=12)
		self.datetime = datetime.datetime(year=int(data[0][13:17]),
			month=int(data[0][18:20]), day=int(data[0][21:23]))
		if (int(data[0][27:29]) >= 0) and (int(data[0][27:29]) <= 23):
			self.datetime = self.datetime + \
			datetime.timedelta(hours=int(data[0][27:29]))
			if (int(data[0][29:31]) >= 0) and (int(data[0][29:31]) <= 59):
				self.datetime = self.datetime + \
				datetime.timedelta(minutes=int(data[0][29:31]))
			if self.datetime - self.systemtime > datetime.timedelta(hours=6):
				self.datetime = self.datetime - datetime.timedelta(days=1)
		else:
			self.datetime = self.systemtime
		self.pressure_source = data[0][38:45]
		self.nonpressure_source = data[0][46:54]
		self.lat = float(data[0][55:62])/10000
		self.lon = float(data[0][63:71])/10000
		self.number_levels = data[0][32:36]
		self.data = data[1:]


class concat():
	###########################################################################
	### Concatenate files in the folder of a given station.					###
	### Define the station you want to concatenate.							###
	### Specify files you want to concatenate.								###
	###########################################################################

	def __init__(self, path_out, stationmeta):
		# Initialize object to determine mandatory information for download
		self.path_out = path_out
		self.stationmeta = stationmeta

	def concat_data(self, files):
		print('\n######### Concate part starts #########')
		# Check which files to concatenate and unzip them if needed
		if files == True:
			filelist = glob(self.path_out+'*'
				+self.stationmeta.stationnumber+'*')
		else:
			filelist = []
			for fname in files:
				if Path(self.path_out+fname).is_file():
					filelist.append(self.path_out+fname)
		filelist_temp = []
		for f in filelist:
			if f[-4:] == '.zip':
				os.system('unzip -o -q '+f+' -d '+self.path_out)
				os.system('rm -f '+f)
				if f[-8:-4] == '.txt':
					filelist_temp.append(f[:-4])
				else:
					continue
			else:
				if f[-4:] == '.txt':
					filelist_temp.append(f)
				else:
					continue
		filelist = filelist_temp
		print('Concatenate following files:')
		[print(f) for f in filelist]		

		# Read in data
		data = {}
		for f in filelist:
			data[f] = open(f).read().splitlines()

		# Get dates of ascends
		ascends = {}
		ascendtimes = []
		for f in filelist:
			ascends[f] = []
			starts = []
			ends = []
			if '-data.txt' in f:
				for n, line in enumerate(data[f]):
					if line.startswith('#'):
						starts.append(n)
						if n != 0:
							ends.append(n-1)
					else:
						continue
				ends.append(n)
				for start, end in zip(starts, ends):
					try:
						ascends[f].append(igra_raw(data[f][start:end+1]))
						ascendtimes.append(ascends[f][-1].systemtime)
					except:
						print('Something is wrong here:')
						print(data[f][start:end+1])
						continue
			elif 'uadb_' in f:
				for n, line in enumerate(data[f]):
					if line.startswith('H'):
						starts.append(n)
						if n != 0:
							ends.append(n-1)
					else:
						continue
				ends.append(n)
				for start, end in zip(starts, ends):
					try:
						ascends[f].append(ucar_raw(data[f][start:end+1]))
						ascendtimes.append(ascends[f][-1].systemtime)
					except:
						print('Something is wrong here:')
						print(data[f][start:end+1])
						continue
		# Define Meta floor
		max_pressure_levels = 0
		max_geopot_levels = 0
		# Find all dates
		ascendtimes = sorted(set(ascendtimes))
		# Create array to save data temporarly
		data_pressure = []
		data_geopot = []
		# Create filenumber array
		fn = [0 for f in filelist]
		for nn, ascendtime in enumerate(ascendtimes):
			avail = []
			for n,f in enumerate(filelist):
				while (ascendtime > ascends[f][fn[n]].systemtime) and \
					(len(ascends[f]) > fn[n]+1):
					fn[n] += 1
				if ascendtime == ascends[f][fn[n]].systemtime:
					avail.append(n)
				elif ascendtime < ascends[f][fn[n]].systemtime:
					pass

			# Extract data out of each ascending
			first = True
			for n, f in zip(avail, [filelist[i] for i in avail]):
				# Ascending Type
				# Pressure in Pa
				# Geopotential Height in m
				# Temperature in  tenth of Deg C
				# Relative Humidity in tenth %
				# Dewpoint Depression in tenth of Deg C
				# Wind Direction in Deg from North
				# Dew point temperature
				asc = ascends[f][fn[n]].data
				if first:
					# IGRA
					if '-data.txt' in f:
						data = [np.array([float(i[0:2]) for i in asc]),
							np.array([float(i[ 9:15]) for i in asc]),
							np.array([float(i[16:21]) for i in asc]),
							np.array([float(i[22:27])/10 for i in asc]),
							np.array([float(i[28:33])/10 for i in asc]),
							np.array([float(i[40:45]) for i in asc]),
							np.array([float(i[46:51])/10 for i in asc]),
							np.array([float(i[34:39])/10 for i in asc])]
						first = False
					# UCAR
					elif 'uadb_' in f:
						data = [np.array([float(i[0:4]) for i in asc]),
							np.array([float(i[ 5:13]) for i in asc]),
							np.array([float(i[14:22]) for i in asc]),
							np.array([float(i[23:29]) for i in asc]),
							np.array([float(i[30:36]) for i in asc]),
							np.array([float(i[37:43]) for i in asc]),
							np.array([float(i[44:50]) for i in asc]),
							np.array([np.nan for i in asc])]
						first = False
				else:
					# IGRA
					if '-data.txt' in f:
						data = [np.append(data[0], np.array([float(i[0:2]) \
							for i in asc])),
							np.append(data[1], np.array([float(i[ 9:15]) \
								for i in asc])),
							np.append(data[2], np.array([float(i[16:21]) \
								for i in asc])),
							np.append(data[3], np.array([float(i[22:27])/10 \
								for i in asc])),
							np.append(data[4], np.array([float(i[28:33])/10 \
								for i in asc])),
							np.append(data[5], np.array([float(i[40:45]) \
								for i in asc])),
							np.append(data[6], np.array([float(i[46:51])/10 \
								for i in asc])),
							np.append(data[7], np.array([float(i[34:39])/10 \
								for i in asc]))]
					# UCAR
					elif 'uadb_' in f:
						data = [np.append(data[0], np.array([float(i[0:4])\
							for i in asc])),
							np.append(data[1],np.array([float(i[ 5:13])
								for i in asc])),
							np.append(data[2], np.array([float(i[14:22])
								for i in asc])),
							np.append(data[3], np.array([float(i[23:29])
								for i in asc])),
							np.append(data[4], np.array([float(i[30:36])
								for i in asc])),
							np.append(data[5], np.array([float(i[37:43])
								for i in asc])),
							np.append(data[6], np.array([float(i[44:50])
								for i in asc])),
							np.append(data[7], np.array([np.nan
								for i in asc]))]

			# Sort data and separate in Pressure and Geopot coordinated
			pressure_ind = [data[1] != -99999.0]
			pressure_sort = data[1][pressure_ind[0]].argsort()
			geopot_ind = [data[2] != -99999.0]
			geopot_sort = data[2][geopot_ind[0]].argsort()
			data_pressure_temp = [data[0][pressure_ind[0]][pressure_sort],
			data[1][pressure_ind[0]][pressure_sort],
			data[2][pressure_ind[0]][pressure_sort],
			data[3][pressure_ind[0]][pressure_sort],
			data[4][pressure_ind[0]][pressure_sort],
			data[5][pressure_ind[0]][pressure_sort],
			data[6][pressure_ind[0]][pressure_sort],
			data[7][pressure_ind[0]][pressure_sort]]
			data_geopot_temp = [data[0][geopot_ind[0]][geopot_sort],
			data[1][geopot_ind[0]][geopot_sort],
			data[2][geopot_ind[0]][geopot_sort],
			data[3][geopot_ind[0]][geopot_sort],
			data[4][geopot_ind[0]][geopot_sort],
			data[5][geopot_ind[0]][geopot_sort],
			data[6][geopot_ind[0]][geopot_sort],
			data[7][geopot_ind[0]][geopot_sort]]
			# Define the levels
			pressure_levels = sorted(set(data_pressure_temp[1]))
			geopot_levels = sorted(set(data_geopot_temp[2]))
			# Find the maximum levels count for the end netcdf
			max_pressure_levels = max(len(pressure_levels),max_pressure_levels)
			max_geopot_levels = max(len(geopot_levels),max_geopot_levels)

			# Create empty arrays to fill and fill with coordinates
			pressure_data_c = np.full([len(pressure_levels), 7], np.nan)
			pressure_data_c[:, 0] = pressure_levels
			geopot_data_c = np.full([len(geopot_levels), 7], np.nan)
			geopot_data_c[:, 1] = geopot_levels
			# Loop over pressure levels
			for n_p, pressure_level in enumerate(pressure_levels):
				# Filter through array and fill in pressure oriented data
				args = np.where(data_pressure_temp[1]==pressure_level)
				# Geopot in Pressure
				for arg in data_pressure_temp[2][args[0]]:
					if arg > -800.0:
						pressure_data_c[n_p, 1] = arg
						break
				# Temperature in Pressure
				for arg in data_pressure_temp[3][args[0]]:
					if arg > -800.0:
						pressure_data_c[n_p, 2] = arg
						break
				# Relative Humidity in Pressure
				for arg in data_pressure_temp[4][args[0]]:
					if arg > -800.0:
						pressure_data_c[n_p, 3] = arg
						break
				# Wind Direction in Pressure
				for arg in data_pressure_temp[5][args[0]]:
					if arg > -800.0:
						pressure_data_c[n_p, 4] = arg
						break
				# Wind Speed in Pressure
				for arg in data_pressure_temp[6][args[0]]:
					if arg > -800.0:
						pressure_data_c[n_p, 5] = arg
						break
				# Dew Point in Pressure
				for arg in data_pressure_temp[7][args[0]]:
					if arg > -800.0:
						pressure_data_c[n_p, 6] = arg
						break
			
			for n_g, geopot_level in enumerate(geopot_levels):
				# Filter through array and fill in geopot oriented data
				args = np.where(data_geopot_temp[2]==geopot_level)
				# Pressure in Geopot
				for arg in data_geopot_temp[1][args[0]]:
					if arg > -800.0:
						geopot_data_c[n_g, 0] = arg
						break
				# Temperature in geopot
				for arg in data_geopot_temp[3][args[0]]:
					if arg > -800.0:
						geopot_data_c[n_g, 2] = arg
						break
				# Relative Humidity in geopot
				for arg in data_geopot_temp[4][args[0]]:
					if arg > -800.0:
						geopot_data_c[n_g, 3] = arg
						break
				# Wind Direction in geopot
				for arg in data_geopot_temp[5][args[0]]:
					if arg > -800.0:
						geopot_data_c[n_g, 4] = arg
						break
				# Wind Speed in geopot
				for arg in data_geopot_temp[6][args[0]]:
					if arg > -800.0:
						geopot_data_c[n_g, 5] = arg
						break
				# Dew Point in geopot
				for arg in data_geopot_temp[7][args[0]]:
					if arg > -800.0:
						geopot_data_c[n_g, 6] = arg
						break
			# Save data in temporary array
			data_pressure.append(pressure_data_c)
			data_geopot.append(geopot_data_c)

		# Create data array for the netcdfs
		array_pressure = np.full([len(ascendtimes), max_pressure_levels, 7],
			fill_value=np.nan)
		array_geopot = np.full([len(ascendtimes), max_geopot_levels, 7],
			fill_value=np.nan)
		array_interpolate = np.full([len(ascendtimes), 1046, 6],
			fill_value=np.nan)
		pressure_levels = np.arange(5.0, 1051.0, 1.0, dtype=np.float32)

		# Loop over acendings
		for ascend_n in np.arange(len(data_pressure)):
			array_pressure[ascend_n, :len(data_pressure[ascend_n]), :] = \
			data_pressure[ascend_n][:, :]
			array_geopot[ascend_n, :len(data_geopot[ascend_n]), :] = \
			data_geopot[ascend_n][:, :]

			# Calculate an interpolation by pressure of the data
			for var_n in np.arange(1,6):
				x_i = np.log(pressure_levels)
				try:
					x = np.log(data_pressure[ascend_n][:,0])\
					[~np.isnan(data_pressure[ascend_n][:,var_n])]
				except RuntimeWarning:
					print('RUNTIMEWARNING with interpolation')
					print(ascend_n)
					print(ascendtimes[ascend_n])
					x = np.log(data_pressure[ascend_n][:,0])\
					[~np.isnan(data_pressure[ascend_n][:,var_n])]
				y = data_pressure[ascend_n][:,var_n]\
				[~np.isnan(data_pressure[ascend_n][:,var_n])]
				if (x.size == 0) or (y.size == 0):
					continue
				array_interpolate[ascend_n, :, var_n-1] = \
				np.interp(x_i, x, y, np.nan, np.nan)

		# Define if it is a PiBal or sounding
		typesounding = np.where(
			(np.all(np.isnan(array_pressure[:, :, 2]), axis=1)) &
			(np.all(np.isnan(array_pressure[:, :, 3]), axis=1)) &
			(np.all(np.isnan(array_pressure[:, :, 6]), axis=1)),
			'PiBal', 'Sounding')

		# Put data in xarray dataset with 4 dimensions:
		# - Time; t, T [datetime]
		# - pressure; p, P [hPa]
		# - pressure_ind; p_ind, P_ind [hPa]
		# - geopot_ind; gph_ind, GPH_ind [m]
		
		result = xr.Dataset({
			# Observation with pressure vertical coordinates
			'pressure_p_obs':
			(['time','p_ind'], array_pressure[:, :, 0]),
			'geopot_p_obs':
			(['time','p_ind'], array_pressure[:, :, 1]),
			'temp_p_obs':
			(['time','p_ind'], array_pressure[:, :, 2]),
			'rh_p_obs':
			(['time','p_ind'], array_pressure[:, :, 3]),
			'wdir_p_obs':
			(['time','p_ind'], array_pressure[:, :, 4]),
			'wspeed_p_obs':
			(['time','p_ind'], array_pressure[:, :, 5]),
			'dp_temp_p_obs':
			(['time','p_ind'], array_pressure[:, :, 6]),
			# Observation with GPH vertical coordinates
			'pressure_gph_obs':
			(['time','gph_ind'], array_geopot[:, :, 0]),
			'geopot_gph_obs':
			(['time','gph_ind'], array_geopot[:, :, 1]),
			'temp_gph_obs':
			(['time','gph_ind'], array_geopot[:, :, 2]),
			'rh_gph_obs':
			(['time','gph_ind'], array_geopot[:, :, 3]),
			'wdir_gph_obs':
			(['time','gph_ind'], array_geopot[:, :, 4]),
			'wspeed_gph_obs':
			(['time','gph_ind'], array_geopot[:, :, 5]),
			'dp_temp_gph_obs':
			(['time','gph_ind'], array_geopot[:, :, 6]),
			# Interpolated values
			'geopot':
			(['time','press'], array_interpolate[:, :, 0]),
			'temp':
			(['time','press'], array_interpolate[:, :, 1]),
			'rh':
			(['time','press'], array_interpolate[:, :, 2]),
			'wdir':
			(['time','press'], array_interpolate[:, :, 3]),
			'wspeed':
			(['time','press'], array_interpolate[:, :, 4]),
			'dp_temp':
			(['time','press'], array_interpolate[:, :, 5]),
			# Meta information
			'typesounding':
			(['time'], typesounding),
			},
			coords=
			{
			'time': ascendtimes,
			'press': pressure_levels,
			'p_ind': np.arange(max_pressure_levels),
			'gph_ind': np.arange(max_geopot_levels),
			}
			)
		result.to_netcdf(self.path_out+'sounding_concat_'+
			str(self.stationmeta.stationnumber)+'.nc')
		return print('########## Concate part ends ##########\n')


class download():
	###########################################################################
	### Download Upperair data from IGRA, UCAR, or any other implemented	###
	### data base.															###
	###########################################################################

	def __init__(self, path_out, platform, stationmeta):
		# Initialize object to determine mandatory information for download
		self.path_out = path_out
		self.platform = platform
		self.stationmeta = stationmeta
		self.force_download = False

	def download_data(self, **kwargs):
		print('\n######### Download part starts #########')
		# Distinguish from which platform to download
		if 'force_download' in kwargs:
			self.force_download = kwargs['force_download']
		else:
			self.force_download = 'ask'
		if self.platform == 'igra':
			download.igra(self)
		elif self.platform == 'ucar':
			if 'credentials' in kwargs:
				try:
					user = kwargs['credentials']['user']
					password = kwargs['credentials']['password']
				except:
					print('Credentials not passed in the right format.')
					print('Pass to download_data dictionary:')
					print("credentials['user'] and credentials['password']")
					return print('########## Download part ends ##########\n')
			else:
				print('UCAR need credentials.')
				print('Pass to download_data dictionary:')
				print("credentials['user'] and credentials['password']")
				return print('########## Download part ends ##########\n')
			download.ucar(self, user=user, password=password)
		else:
			print('Downloading from '+self.platform+' is not implemented.\n'+\
				'Implement in functions/download!')
			return print('########## Download part ends ##########\n')
		return print('########## Download part ends ##########\n')

	def ucar(obj, user, password):
		# To Download from the Research Data Archive run by NCAR you need an
		# account to access the database. The data is free to use with credit
		# to NCAR.
		# Register at:
		# https://rda.ucar.edu/index.html?hash=data_user&action=register

		# Help function to download from NCAR research server
		def check_file_status(filepath, filesize):
			sys.stdout.write('\r')
			sys.stdout.flush()
			size = int(os.stat(filepath).st_size)
			percent_complete = (size/filesize)*100
			sys.stdout.write('%.3f %s' % (percent_complete, '% Completed'))
			sys.stdout.flush()
			return

		# Access and login to the database
		url = 'https://rda.ucar.edu/cgi-bin/login'
		values = {'email' : user, 'passwd' : password, 'action' : 'login'}
		# Authenticate
		ret = requests.post(url, data=values)
		if ret.status_code != 200:
			print('Bad Authentication')
			print(ret.text)
			return

		# Check if files are already available
		f1 = 'uadb_trhc_'+obj.stationmeta.stationnumber+'.txt'
		f2 = 'uadb_trh_'+obj.stationmeta.stationnumber+'.txt'
		f3 = 'uadb_windc_'+obj.stationmeta.stationnumber+'.txt'
		f4 = 'uadb_wind_'+obj.stationmeta.stationnumber+'.txt'
		filelist = [f1,f2,f3,f4]
		# Check if path and file exist
		if not Path(obj.path_out).is_dir():
			print('Output path does not exist:')
			print(obj.path_out)
			return
		filelist_temp = []
		for fname in filelist:
			if Path(obj.path_out+fname).is_file() and \
				('ask' == obj.force_download):
				print(fname+' is already downloaded with timestamp:')
				print(dt.datetime.fromtimestamp(
					(os.stat(obj.path_out+fname)).st_ctime))
				dec = input('Do you want to download anyway? [y/n]')
				if not dec == 'y':
					continue
				else:
					filelist_temp.append(fname)
			elif Path(obj.path_out+fname).is_file() and \
				(not obj.force_download):
				continue
			else:
				filelist_temp.append(fname)
		filelist = filelist_temp
		if len(filelist) == 0:
			return print('No files from UCAR will be downloaded.')
		else:
			print('Following files will be downloaded if available.')

		# Download files
		serverpath = 'https://rda.ucar.edu/data/ds370.1/'
		for fname in filelist:
			req = requests.get(serverpath+fname, cookies = ret.cookies,
				allow_redirects=True, stream=True)
			print('Download:\n'+fname)
			print('At:\n'+obj.path_out+fname)
			if req.status_code != 200:
				print('No '+fname+' on the server, continue to next file.')
				continue
			filesize = int(req.headers['Content-length'])
			with open(obj.path_out+fname, 'wb') as outfile:
				chunk_size=1048576
				for chunk in req.iter_content(chunk_size=chunk_size):
					outfile.write(chunk)
					if chunk_size < filesize:
						check_file_status(obj.path_out+fname, filesize)
			check_file_status(obj.path_out+fname, filesize)
		return

	def igra(obj):
		# Download from Integrated Global Radiosonde Archive (IGRA).

		# Check if path and file exist
		if not Path(obj.path_out).is_dir():
			print('Output path does not exist:')
			print(obj.path_out)
			return
		fname = obj.stationmeta.country+'000'+obj.stationmeta.stationnumber\
			+'-data.txt.zip'

		if (Path(obj.path_out+fname).is_file() or \
			Path(obj.path_out+fname[:-4]).is_file()) and \
			('ask' == obj.force_download):
			print(fname+' is already downloaded with timestamp:')
			print(dt.datetime.fromtimestamp(
				(os.stat(obj.path_out+fname)).st_ctime))
			dec = input('Do you want to download anyway? [y/n]')
			if not dec == 'y':
				return print('No files from IGRA will be downloaded.')
			else:
				pass
		elif (Path(obj.path_out+fname).is_file() or \
			Path(obj.path_out+fname[:-4]).is_file()) and \
			(not obj.force_download):
			return print('No files from IGRA will be downloaded.')
		else:
			pass
		
		# Orientation for FTP server of IGRA
		ftp = FTP('ftp.ncdc.noaa.gov')
		ftp.login()
		ftp.cwd('/pub/data/igra/data/data-por/')
		filevar = open(obj.path_out+fname, 'wb')
		try:
			ftp.retrbinary('RETR '+fname, filevar.write)
			print('Download succesfull:\n'+fname)
			print('At:\n'+obj.path_out+fname)
		except:
			print(fname+' is not on noaa-igra server')
			ftp.quit()
			filevar.close()
		filevar.close()
		ftp.quit()
		return

