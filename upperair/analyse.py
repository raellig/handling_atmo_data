import sys
import xarray as xr
import numpy as np
import datetime
from matplotlib import pyplot as plt
import matplotlib as mpl
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.gridspec import GridSpec
import re

class meta():
	def __init__(self, path_data, stationmeta):
		# Initialize object
		self.path_data = path_data
		self.stationmeta = stationmeta


	def get_avail_dates_types(self):
		# Function to get the available dates
		data = xr.open_dataset(self.path_data+'sounding_concat_'+
			self.stationmeta.stationnumber+'.nc')
		self.typesounding = data.typesounding
		return

class visualize():
	def availability_one_station(obj, start, end, **kwargs):
		# This Function plots the availability of upperair data between the
		# given range of year
		# Requiered input:
		# - obj of init and get_avail_dates_types called
		# - start and end of period in years
		# - opt: path to save figure instead of prompt

		# Get opt keywords
		if 'path_figures' in kwargs:
			prompt = False
			obj.path_figures = kwargs['path_figures']
		else:
			prompt = True

		# Prepare figure
		fig = plt.figure(figsize=(9,7))
		fig.suptitle('Availability of upperair Soundings and PiBals\n'+
			'at station '+obj.stationmeta.name+' ('+
			obj.stationmeta.stationnumber+')')
		# Find arangement of figure
		diff = end - start
		x = 1
		y = 1
		alt = True # True = y
		while diff > x * y:
			if alt:
				y += 1
				alt = False
			else:
				x += 1
				alt = True

		# Create Subplot handling
		gs = GridSpec(y, x, figure=fig, left=0.09, right=0.95, top=0.9, bottom=0.2)

		# Loop over years and plots
		axs = {}
		for n, year in enumerate(np.arange(start,end)):
			# Array of zeros of months and days matrix
			soundings = np.zeros((12*2, 31*2))
			axs[year] = fig.add_subplot(gs[int((n-(n%x))/x), n%x])
			# Define time range from to
			time_0 = datetime.datetime.strptime(
				str(year)+'-01-01T00:00:00', '%Y-%m-%dT%H:%M:%S')
			time_1 = datetime.datetime.strptime(
				str(year)+'-01-01T05:00:00', '%Y-%m-%dT%H:%M:%S')
			data = obj.typesounding
			while time_0.year < year + 1:
				if obj.typesounding.sel(
					time=slice(time_0, time_1)).values.size == 0:
					pass
				elif 'Sounding' in obj.typesounding.sel(
					time=slice(time_0, time_1)).values:
					if (time_0.hour == 6) or (time_0.hour == 18):
						x_offset = 1
					else:
						x_offset = 0
					if (time_0.hour == 12) or (time_0.hour == 18):
						y_offset = 1
					else:
						y_offset = 0
					soundings[time_0.month*2+y_offset-2, \
						time_0.day*2+x_offset-2] = 5 + time_0.hour/6
				elif 'PiBal' in obj.typesounding.sel(
					time=slice(time_0, time_1)).values:
					# Make offset depending on hour
					if (time_0.hour == 6) or (time_0.hour == 18):
						x_offset = 1
					else:
						x_offset = 0
					if (time_0.hour == 12) or (time_0.hour == 18):
						y_offset = 1
					else:
						y_offset = 0
					soundings[time_0.month*2+y_offset-2, \
						time_0.day*2+x_offset-2] = 1 + time_0.hour/6
				else:
					pass
				# Go to next time slice
				time_0 = time_0 + datetime.timedelta(hours=6)
				time_1 = time_1 + datetime.timedelta(hours=6)
			# Handling plots
			colors = ['white', 'lightsalmon', 'coral', 'red', 'indianred',\
				'cyan', 'deepskyblue', 'dodgerblue', 'mediumblue']
			cmap = mpl.colors.ListedColormap(colors)
			bounds = np.arange(-0.5, 9.0, 1.0)
			norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
			# Plot it
			axs[year].imshow(soundings, interpolation='none', cmap=cmap, \
				extent=(1,32,13,1), norm=norm)

			# Handling axes
			axs[year].set_yticks(np.arange(1,13), minor=True)
			axs[year].set_yticks(np.arange(1,12,3), minor=False)
			axs[year].set_xticks(np.arange(1,32), minor=True)
			axs[year].set_xticks(np.arange(1,31,7), minor=False)
			axs[year].yaxis.grid(True, which='minor')
			axs[year].yaxis.grid(True, which='major', 
				linewidth=1.2, color='black')
			axs[year].xaxis.grid(True, which='minor')
			axs[year].xaxis.grid(True, which='major', 
				linewidth=1.2, color='black')
			# For left handside labeling
			if n % x == 0:
				axs[year].set_ylabel('Months')
			if (n+1) > (y*x-x):
				axs[year].set_xlabel('Days')

			# Set subplot title
			axs[year].set_title(str(year))

		labels = ['No Observation']
		labels.extend([soundingtype+' at '+str(hour)+' UTC' \
			for soundingtype in ['PiBal', 'Radiosounding'] \
			for hour in np.arange(0,24,6)])
		legend_elements = [Patch(facecolor=color, label=label, \
			edgecolor='grey') for color, label in zip(colors, labels)]

		# Create the figure
		fig.legend(handles=legend_elements, loc='upper left', fancybox=True, 
			shadow=True, bbox_to_anchor=(0.1, 0.15), ncol=3)

		# Handling Plot
		if prompt:
			plt.show()
		else:
			plt.savefig(obj.path_figures+'available_soundings_'+
				obj.stationmeta.stationnumber+'_'+str(start)+'_'+str(end))
		plt.close()
		return

